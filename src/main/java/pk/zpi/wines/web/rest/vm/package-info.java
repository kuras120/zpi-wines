/**
 * View Models used by Spring MVC REST controllers.
 */
package pk.zpi.wines.web.rest.vm;
